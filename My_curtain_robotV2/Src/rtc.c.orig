/**
  ******************************************************************************
  * File Name          : RTC.c
  * Description        : This file provides code for the configuration
  *                      of the RTC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rtc.h"

/* USER CODE BEGIN 0 */
#include "stdio.h"
#include "string.h"
RTC_DateTypeDef sdatestructure;
RTC_TimeTypeDef stimestructure;
/* USER CODE END 0 */

RTC_HandleTypeDef hrtc;

/* RTC init function */
void MX_RTC_Init(void)
{
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /** Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date 
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the WakeUp 
  */
  if (HAL_RTCEx_SetWakeUpTimer(&hrtc, 0, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_RTC_MspInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspInit 0 */

  /* USER CODE END RTC_MspInit 0 */
    /* RTC clock enable */
    __HAL_RCC_RTC_ENABLE();
  /* USER CODE BEGIN RTC_MspInit 1 */

  /* USER CODE END RTC_MspInit 1 */
  }
}

void HAL_RTC_MspDeInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspDeInit 0 */

  /* USER CODE END RTC_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_RTC_DISABLE();
  /* USER CODE BEGIN RTC_MspDeInit 1 */

  /* USER CODE END RTC_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
uint32_t time2Stamp(void)    //北京时间转时间戳
{
    uint32_t result = 0;
    uint16_t Year = 0;
    //const uint8_t Days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    const uint16_t monDays[12] = {0,31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};
    /* Get the RTC current Time ,must get time first*/
    HAL_RTC_GetTime(&hrtc, &stimestructure, RTC_FORMAT_BIN);
    /* Get the RTC current Date */
    HAL_RTC_GetDate(&hrtc, &sdatestructure, RTC_FORMAT_BIN);
    /* Display date Format : yy/mm/dd */
    printf("%02d/%02d/%02d\r\n",2000 + sdatestructure.Year, sdatestructure.Month, sdatestructure.Date);
    /* Display time Format : hh:mm:ss */
    printf("%02d:%02d:%02d\r\n",stimestructure.Hours, stimestructure.Minutes, stimestructure.Seconds);
	
    Year=sdatestructure.Year+2000;
    result = (Year - 1970) * 365 * 24 * 3600 + (monDays[sdatestructure.Month-1] + sdatestructure.Date - 1) * 24 * 3600 + (stimestructure.Hours-8) * 3600 + stimestructure.Minutes * 60 + stimestructure.Seconds;
    printf("[%u]",result);
    result += (sdatestructure.Month>2 && (Year % 4 == 0) && (Year % 100 != 0 || Year % 400 == 0))*24*3600;	//闰月
    printf("[%u]",result);
    Year -= 1970;
    result += (Year/4 - Year/100 + Year/400)*24 * 3600;		//闰年
	printf("[%u]",result);
	printf("时间戳Success\r\n");
    return result;
}
void Calibration_Times(char * Time_buf)
{

	 struct tm *gm_date;
	uint8_t RTC_Buf[6]= {0};
    RTC_TimeTypeDef sTime = {0};
    RTC_DateTypeDef sDate = {0};
	uint32_t time_buf = atoi((char *)Time_buf);
     gm_date = localtime(&time_buf);
	 printf("year:%d\r\n",gm_date->tm_year);
        RTC_Buf[0] = gm_date->tm_year-100;
        RTC_Buf[1] = gm_date->tm_mon+1;
        RTC_Buf[2] = gm_date->tm_mday;
        RTC_Buf[3] = gm_date->tm_hour+8;
        RTC_Buf[4] = gm_date->tm_min;
        RTC_Buf[5] = gm_date->tm_sec;
        HAL_PWR_EnableBkUpAccess();
        if(HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR1)!=0x3050)
        {
            HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x5050);
            sTime.Hours = RTC_Buf[3];
            sTime.Minutes = RTC_Buf[4];
            sTime.Seconds = RTC_Buf[5];
            if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
            {
				printf("Calibration_Times error...\r\n");
                Error_Handler();
            }
            sDate.WeekDay = RTC_WEEKDAY_MONDAY;
            sDate.Month = RTC_Buf[1];
            sDate.Date = RTC_Buf[2];
            sDate.Year = RTC_Buf[0];
            if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
            {
				printf("Calibration_Times error...\r\n");
                Error_Handler();
            }
        }
        printf("Calibration_Times success...\r\n");
}

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
